//! A basic example that generates and publishes a DID Document and purges it after,
//!
//! cargo run --example purge_did

use identity::core::Timestamp;
use identity::crypto::ProofOptions;
use identity::iota::ExplorerUrl;
use identity::iota::Receipt;
use identity::prelude::*;

pub async fn run() -> Result<(IotaDocument, KeyPair, Receipt)> {
    // Create a client instance to send messages to the Tangle.
    let client: Client = Client::new().await?;

    // Generate two new Ed25519 public/private key pairs.
    let keypair_1: KeyPair = KeyPair::new(KeyType::Ed25519)?;

    // Create a DID Document (an identity) from the generated key pair.
    let mut document: IotaDocument = IotaDocument::new(&keypair_1)?;

    // Sign the DID Document with the default signing method.
    document.sign_self(
        keypair_1.private(),
        document.default_signing_method()?.id().clone(),
    )?;

    // Publish the DID Document to the Tangle.
    let first_receipt: Receipt = client.publish_document(&document).await?;
    // println!("Publish Receipt > {:#?}", receipt);

    // Display the web explorer url that shows the published message.
    // let explorer: &ExplorerUrl = ExplorerUrl::mainnet();
    // println!(
    //   "DID Document Transaction > {}",
    //   explorer.message_url(first_receipt.message_id())?
    // );
    // println!("Explore the DID Document > {}", explorer.resolver_url(document.id())?);

    // Clone the document so we can sign with the valid method
    let old_document = document.clone();

    // remove default signing method and purge document
    document.remove_method(&document.default_signing_method()?.id().clone())?;
    document.metadata.previous_message_id = *first_receipt.message_id();
    document.metadata.updated = Some(Timestamp::now_utc());

    // println!("Removed method DID Document JSON > {:#}", document);

    // Sign the new document with the old key
    old_document.sign_data(
        &mut document,
        keypair_1.private(),
        old_document.default_signing_method()?.id().clone(),
        ProofOptions::new(),
    )?;
    //  Publish the purged DID Document to the Tangle.
    let second_receipt: Receipt = client.publish_document(&document).await?;
    // println!("old_doc > {:#}", old_document);
    // println!("new_doc > {:#}", document);

    // Display the web explorer url that shows the published message.
    let explorer: &ExplorerUrl = ExplorerUrl::mainnet();
    println!(
        "purged DID Document Transaction > {}",
        explorer.message_url(second_receipt.message_id())?
    );
    println!(
        "Purged did document > {}",
        explorer.resolver_url(document.id())?
    );

    // Retrieve the updated message history of the DID.
    // let history: DocumentHistory = client.resolve_history(document.id()).await?;
    // println!("History = {:#?}", history);

    // Resolve Document from Tangle
    let resolved_doc = client.read_document(document.id()).await?;
    // println!("Resolved = {:#?}", resolved_doc);

    // Document is invalid, because it was purged
    assert!(resolved_doc
        .document
        .verify_document(&resolved_doc.document)
        .is_err());

    // Since we can't add a new capability invacation through code we need to construct a new document and push it to the tangle

    let mut new_document: IotaDocument = IotaDocument::new(&keypair_1)?;
    // We need to change this parameters so that we can build a new valid document
    new_document.metadata.created = document.metadata.created;
    new_document.metadata.updated = Some(Timestamp::now_utc());
    new_document.metadata.previous_message_id = *second_receipt.message_id();
    // Sign the DID Document with the default signing method.
    new_document.sign_self(
        keypair_1.private(),
        new_document.default_signing_method()?.id().clone(),
    )?;

    println!("doc before pushing to tangle > {:#}", new_document);

    let third_receipt: Receipt = client.publish_document(&new_document).await?;

    println!(
        "revived DID Document Transaction > {}",
        explorer.message_url(third_receipt.message_id())?
    );
    println!(
        "revived did document > {}",
        explorer.resolver_url(document.id())?
    );
    // Try to resolve the new_doc
    let resolved_doc = client.read_document(new_document.id()).await?;
    // Document is invalid, because it was purged
    assert!(resolved_doc
        .document
        .verify_document(&resolved_doc.document)
        .is_err());
    println!(
        "validation result: {:?}",
        resolved_doc
            .document
            .verify_document(&resolved_doc.document)
    );
    Ok((document, keypair_1, first_receipt))
}

#[allow(dead_code)]
#[tokio::main]
async fn main() -> Result<()> {
    let _ = run().await?;

    Ok(())
}
