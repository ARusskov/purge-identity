//! A basic example that generates and publishes a DID Document and purges it after,
//!
//! cargo run --example purge_did

use identity::core::json;
use identity::core::FromJson;
use identity::core::Timestamp;
use identity::core::Url;
use identity::credential::Credential;
use identity::credential::CredentialBuilder;
use identity::credential::Subject;
use identity::crypto::ProofOptions;
use identity::did::DID;
use identity::iota::CredentialValidationOptions;
use identity::iota::CredentialValidator;
use identity::iota::ExplorerUrl;
use identity::iota::Receipt;
use identity::iota::Resolver;
use identity::prelude::*;

pub async fn run() -> Result<(IotaDocument, KeyPair, Receipt)> {
    // Create a client instance to send messages to the Tangle.
    let client: Client = Client::new().await?;

    // Generate two new Ed25519 public/private key pairs.
    let keypair_1: KeyPair = KeyPair::new(KeyType::Ed25519)?;

    // Create a DID Document (an identity) from the generated key pair.
    let mut document: IotaDocument = IotaDocument::new(&keypair_1)?;

    // Sign the DID Document with the default signing method.
    document.sign_self(
        keypair_1.private(),
        document.default_signing_method()?.id().clone(),
    )?;

    // Publish the DID Document to the Tangle.
    let first_receipt: Receipt = client.publish_document(&document).await?;
    // println!("Publish Receipt > {:#?}", receipt);

    // Create an unsigned Credential with claims about `subject` specified by `issuer`.
    let mut credential: Credential = issue_degree(&document, &document)?;

    // Sign the Credential with the issuer's #newKey private key, so we can later revoke it
    document.sign_data(
        &mut credential,
        keypair_1.private(),
        document.default_signing_method()?.id(),
        ProofOptions::default(),
    )?;

    // Check the verifiable credential
    let resolver: Resolver = Resolver::new().await?;
    let resolved_issuer_doc = resolver.resolve_credential_issuer(&credential).await?;
    let validation_result: Result<()> = CredentialValidator::validate(
        &credential,
        &resolved_issuer_doc,
        &CredentialValidationOptions::default(),
        identity::iota::FailFast::FirstError,
    )
    .map_err(Into::into);

    println!("VC validation result: {:?}", validation_result);

    // Clone the document so we can sign with the valid method
    let old_document = document.clone();

    // remove default signing method and purge document
    document.remove_method(&document.default_signing_method()?.id().clone())?;
    document.metadata.previous_message_id = *first_receipt.message_id();
    document.metadata.updated = Some(Timestamp::now_utc());

    // println!("Removed method DID Document JSON > {:#}", document);

    // Sign the new document with the old key
    old_document.sign_data(
        &mut document,
        keypair_1.private(),
        old_document.default_signing_method()?.id().clone(),
        ProofOptions::new(),
    )?;
    //  Publish the purged DID Document to the Tangle.
    let second_receipt: Receipt = client.publish_document(&document).await?;
    // println!("old_doc > {:#}", old_document);
    // println!("new_doc > {:#}", document);

    // Display the web explorer url that shows the published message.
    let explorer: &ExplorerUrl = ExplorerUrl::mainnet();
    println!(
        "DID Document Transaction > {}",
        explorer.message_url(second_receipt.message_id())?
    );
    println!(
        "Purged did document > {}",
        explorer.resolver_url(document.id())?
    );

    // Check the verifiable credential
    let resolver: Resolver = Resolver::new().await?;
    let resolved_issuer_doc = resolver.resolve_credential_issuer(&credential).await?;
    let validation_result: Result<()> = CredentialValidator::validate(
        &credential,
        &resolved_issuer_doc,
        &CredentialValidationOptions::default(),
        identity::iota::FailFast::FirstError,
    )
    .map_err(Into::into);

    println!("VC validation result: {:?}", validation_result);
    assert!(validation_result.is_err());
    println!("Credential successfully revoked!");
    Ok((document, keypair_1, first_receipt))
}
/// Helper that takes two DID Documents (identities) for issuer and subject, and
/// creates an unsigned credential with claims about subject by issuer.
pub fn issue_degree(issuer: &IotaDocument, subject: &IotaDocument) -> Result<Credential> {
    // Create VC "subject" field containing subject ID and claims about it.
    let subject: Subject = Subject::from_json_value(json!({
      "id": subject.id().as_str(),
      "name": "Alice",
      "degree": {
        "type": "BachelorDegree",
        "name": "Bachelor of Science and Arts",
      },
      "GPA": "4.0",
    }))?;

    // Build credential using subject above and issuer.
    let credential: Credential = CredentialBuilder::default()
        .id(Url::parse("https://example.edu/credentials/3732")?)
        .issuer(Url::parse(issuer.id().as_str())?)
        .type_("UniversityDegreeCredential")
        .subject(subject)
        .build()?;

    Ok(credential)
}
#[allow(dead_code)]
#[tokio::main]
async fn main() -> Result<()> {
    let _ = run().await?;

    Ok(())
}
